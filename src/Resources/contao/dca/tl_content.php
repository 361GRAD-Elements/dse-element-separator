<?php

/**
 * 361GRAD Element Separator
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

// Element palettes
$GLOBALS['TL_DCA']['tl_content']['palettes']['dse_separator'] =
    '{type_legend},type;' .
    '{line_legend},dse_linecolor;' .
    '{margin_legend},dse_marginTop,dse_marginBottom;' .
    '{invisible_legend:hide},invisible,start,stop';

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_marginTop']    = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_marginTop'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_marginBottom'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "text NULL"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_linecolor'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_linecolor'],
    'default'   => 'none',
    'inputType' => 'radio',
    'options'   => [
        'none',
        // #141312
        'black',
        // #8D8986
        'middlegrey',
        // #C6C4C2
        'lightgrey',
        // #E3E2E2
        'ultralightgrey',
        // #F7F7F7
        'naturalwhite'
    ],
    'reference' => [
        'none'           => Dse\ElementsBundle\ElementSeparator\Element\ContentDseSeparator::refColor(
            '#FFF',
            '#000',
            'Keine'
        ),
        'black'          => Dse\ElementsBundle\ElementSeparator\Element\ContentDseSeparator::refColor(
            '#141312',
            '#FFF',
            'Black'
        ),
        'middlegrey'     => Dse\ElementsBundle\ElementSeparator\Element\ContentDseSeparator::refColor(
            '#8D8986',
            '#FFF',
            'MiddleGrey'
        ),
        'lightgrey'      => Dse\ElementsBundle\ElementSeparator\Element\ContentDseSeparator::refColor(
            '#C6C4C2',
            '#000',
            'LightGrey'
        ),
        'ultralightgrey' => Dse\ElementsBundle\ElementSeparator\Element\ContentDseSeparator::refColor(
            '#E3E2E2',
            '#000',
            'UltralightGrey'
        ),
        'naturalwhite'   => Dse\ElementsBundle\ElementSeparator\Element\ContentDseSeparator::refColor(
            '#F7F7F7',
            '#000',
            'NaturalWhite'
        )
    ],
    'eval'      => [
        'tl_class' => 'clr'
    ],
    'sql'       => "varchar(32) NOT NULL default ''"
];
