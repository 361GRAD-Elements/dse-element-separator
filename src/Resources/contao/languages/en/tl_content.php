<?php

/**
 * 361GRAD Element Separator
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements']  = 'DSE-Elements';
$GLOBALS['TL_LANG']['CTE']['dse_separator'] = ['Separator', 'Separator'];

$GLOBALS['TL_LANG']['tl_content']['line_legend']   = 'Line Settings';
$GLOBALS['TL_LANG']['tl_content']['dse_linecolor']   = ['Color', 'Here you can select the color of the separator'];

$GLOBALS['TL_LANG']['tl_content']['margin_legend']   = 'Margin Settings';
$GLOBALS['TL_LANG']['tl_content']['dse_marginTop']   = ['Margin Top', 'Here you can add Margin to the top edge of the element (numbers only)'];
$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom']   = ['Margin Bottom', 'Here you can add Margin to the bottom edge of the element (numbers only)'];
