<?php

/**
 * 361GRAD Element Separator
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

namespace Dse\ElementsBundle\ElementSeparator\Element;

use Contao\BackendTemplate;
use Contao\ContentElement;
use Patchwork\Utf8;

/**
 * Class ContentDseSeparator
 *
 * @package Dse\ElementsBundle\Elements
 */
class ContentDseSeparator extends ContentElement
{
    /**
     * Template name.
     *
     * @var string
     */
    protected $strTemplate = 'ce_dse_separator';


    /**
     * Display a wildcard in the back end.
     *
     * @return string
     */
    public function generate()
    {
        if (TL_MODE == 'BE') {
            $objTemplate = new BackendTemplate('be_wildcard');

            $objTemplate->wildcard = '### ' . Utf8::strtoupper($GLOBALS['TL_LANG']['CTE']['dse_separator'][1]) . ' ###';
            $objTemplate->title    = $this->headline;

            return $objTemplate->parse();
        }

        return parent::generate();
    }


    /**
     * Generate the module
     *
     * @return void
     */
    protected function compile()
    {
    }

    /**
     * Creates special label for checkboxes with a colored style
     *
     * @param string $bgc   The Background Color.
     * @param string $fgc   The Foreground Color.
     * @param string $title The Label Title.
     *
     * @return string
     */
    public static function refColor($bgc, $fgc, $title)
    {
        $style = 'display:inline-block;width:100px;text-align:center;';
        return '<span style="background-color:' . $bgc . ';color:' . $fgc . ';' . $style . '">' . $title . '</span>';
    }
}
